let log = require('@dosarrest/loopback-component-logger')('perspective-auth/boot');
class Boot {
  constructor(app, done) {
    let me = this;
    me.app = app;
    me.done = done;
    me.init();
  }
  init() {
    let me = this;
    let done = me.done;
    let app = me.app;
    done();
  }
}
module.exports = Boot;
