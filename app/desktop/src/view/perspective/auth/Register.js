Ext.define('Client.view.perspective.auth.Register', {
  extend: 'Client.view.perspective.auth.Base',
  xtype: 'perspective.auth.register',

  requires: [
    'Ext.field.Checkbox',
    'Ext.field.Email',
    'Ext.field.Password',
    'Ext.layout.HBox',
    'Ext.Spacer',
    'Ext.data.validator.Email',
    'Ext.data.validator.Presence'
  ],

  items: [{
    xtype: 'panel',
    bodyPadding: 20,
    defaults: {
      margin:'0 0 10 0'
    },
    items: [{
      xtype: 'component',
      html: 'Create an Account'
    }, {
      xtype: 'textfield',
      reference: 'registerName',
      validators: 'presence',
      required: true,
      placeholder: 'Full Name'
    }, {
      xtype: 'textfield',
      reference: 'registerUsername',
      validators: 'presence',
      required: true,
      placeholder: 'Username'
    }, {
      xtype: 'emailfield',
      reference: 'registerEmail',
      validators: ['presence', 'email'],
      required: true,
      placeholder: 'Email'
    }, {
      xtype: 'passwordfield',
      reference: 'registerPassword',
      validators: 'presence',
      required: true,
      placeholder: 'Password'
    }, {
      xtype: 'checkboxfield',
      boxLabel: 'I agree to the terms & conditions'
    }, {
      xtype: 'button',
      text: 'Signup',
      iconAlign: 'right',
      iconCls: 'x-fa fa-user-plus',
      ui: 'confirm alt rounded raised',
      width: '100%',
      handler: 'registerHandler'
    }, {
      xtype: 'button',
      margin: 0,
      text: 'Login with Facebook',
      iconAlign: 'right',
      iconCls: 'x-fa fa-facebook',
      ui: 'facebook rounded raised',
      width: '100%',
      handler: 'facebookLoginHandler'
    }, {
      xtype: 'component',
      margin: 0,
      html: '<a href="#auth/login" class="perspective-auth-link">Back to Login</a>'
    }]
  }]
});
