Ext.define('Client.view.perspective.auth.Base', {
  extend: 'Ext.form.Panel',
  xtype: 'perspective.auth.base',
  requires: [
    'Ext.layout.VBox'
  ],
  controller: {type: 'perspective.auth.base'},
  baseCls: 'perspective-auth-base',
  layout: {
    type: 'vbox',
    align: 'center',
    pack: 'center'
  }
});
