Ext.define('Client.view.perspective.auth.BaseController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.perspective.auth.base',
  requires: [
    'Ext.Ajax'
  ],

  passResetHandler: function () {
    let me = this;
    let params = {
      password: me.lookup('passResetEmail').getValue(),
    };   let view = me.getView();
    if (view.validate()) {
      console.log('Params', params);
      this.redirectTo('main');
    } else {
      console.log('Form Invalid Params', params);
    }
  },

  unlockHandler: function () {
    let me = this;
    let view = me.getView();
    let params = {
      password: me.lookup('unlockPassword').getValue(),
    };
    if (view.validate()) {
      console.log('Params', params);
      this.redirectTo('main');
    } else {
      console.log('Form Invalid Params', params);
    }
  },

  loginKeyup: function(field, e, eOpts) {
    if (e.getKey() === e.ENTER) {
      this.loginHandler();
    }
  },
  loginHandler: function () {
    let me = this;
    let view = me.getView();
    let Authentication = Client.app.getController('Authentication');
    let Loader = Client.app.getController('Loader');
    let params = {
      email: me.lookup('loginEmail').getValue(),
      password: me.lookup('loginPassword').getValue(),
    };
    if (view.validate()) {
      // console.log('Params', params);
      Authentication.localLogin(params.email, params.password).then((success) => {
        if (success === true) {
          if (Client.app.originalHash) {
            me.redirectTo(Client.app.originalHash);
          } else if (Client.app.hash && !_.startsWith(Client.app.hash, 'auth')) {
            me.redirectTo(Client.app.hash);
          } else {
            me.redirectTo(Loader.serverInfo.defaultToken);
          }
        } else {
          Ext.toast(success);
        }
      }).catch(err => {
        console.error(err);
      });
    } else {
      console.log('Form Invalid Params', params);
    }
  },

  registerHandler: function () {
    let me = this;
    let view = me.getView();
    let params = {
      name: me.lookup('registerName').getValue(),
      username: me.lookup('registerUsername').getValue(),
      email: me.lookup('registerEmail').getValue(),
      password: me.lookup('registerPassword').getValue(),
    };
    if (view.validate()) {
      console.log('Params', params);
      this.redirectTo('main');
    } else {
      console.log('Form Invalid Params', params);
    }
  },

  facebookLoginHandler: function () {
    this.redirectTo('main');
  },

  goToDashboard: function () {
    this.redirectTo('main');
  },

  goToRegister: function () {
    this.redirectTo('auth/register');
  }
});
