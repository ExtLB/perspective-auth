Ext.define('Client.view.perspective.auth.Login', {
  extend: 'Client.view.perspective.auth.Base',
  xtype: 'perspective.auth.login',

  requires: [
    'Ext.field.Checkbox',
    'Ext.field.Email',
    'Ext.field.Password',
    'Ext.layout.HBox',
    'Ext.Spacer',
    'Ext.data.validator.Email',
    'Ext.data.validator.Presence'
  ],
  defaultFocus: 'panel#emailfield',
  items: [{
    xtype: 'panel',
    padding: 20,
    width: 300,
    defaults: {
      margin: '0 0 10 0'
    },
    items: [{
      html: 'Sign into your account'
    }, {
      xtype: 'emailfield',
      reference: 'loginEmail',
      validators: ['presence', 'email'],
      required: true,
      placeholder: 'Email',
      listeners: {
        keyup: 'loginKeyup',
      },
    }, {
      xtype: 'passwordfield',
      reference: 'loginPassword',
      validators: 'presence',
      required: true,
      placeholder: 'Password',
      listeners: {
        keyup: 'loginKeyup',
      },
    }, {
      layout: 'hbox',
      items: [{
        xtype: 'checkboxfield',
        boxLabel: 'Remember Me'
      }, {
        xtype: 'spacer'
      }, {
        xtype: 'component',
        html: '<a href="#auth/passwordreset" class="perspective-auth-link">Forgot Password</a>',
        margin: '7 0 0 45'
      }]
    }, {
      xtype: 'button',
      width: '100%',
      text: 'Login',
      iconAlign: 'right',
      iconCls: 'x-fa fa-angle-right',
      ui: 'confirm alt rounded raised',
      handler: 'loginHandler'
    }, {
      xtype: 'button',
      width: '100%',
      text: 'Login with Facebook',
      iconAlign: 'right',
      iconCls: 'x-fa fa-facebook',
      ui: 'facebook rounded raised',
      handler: 'facebookLoginHandler'
    }, {
      xtype: 'button',
      width: '100%',
      margin: 0,
      text: 'Create Account',
      ui: 'gray-button rounded raised',
      iconAlign: 'right',
      iconCls: 'x-fa fa-user-plus',
      handler: 'goToRegister'
    }]
  }]
});

