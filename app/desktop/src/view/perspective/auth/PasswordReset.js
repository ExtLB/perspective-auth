Ext.define('Client.view.perspective.auth.PasswordReset', {
  extend: 'Client.view.perspective.auth.Base',
  xtype: 'perspective.auth.passwordreset',

  requires: [
    'Ext.field.Email',
    'Ext.data.validator.Email',
    'Ext.data.validator.Presence'
  ],

  items: [{
    xtype: 'panel',
    bodyPadding: 20,
    defaults: {
      margin: '0 0 10 0'
    },
    items: [{
      xtype: 'component',
      html: 'Forgot Password'
    }, {
      xtype: 'emailfield',
      reference: 'passResetEmail',
      validators: ['presence', 'email'],
      required: true,
      minWidth: 300,
      placeholder: 'example@example.com'
    }, {
      xtype: 'button',
      text: 'Reset Password',
      iconAlign: 'right',
      iconCls: 'x-fa fa-angle-right',
      ui: 'action rounded raised',
      width: '100%',
      handler: 'passResetHandler'
    }, {
      xtype: 'component',
      margin: 0,
      html: '<a href="#auth/login" class="perspective-auth-link">Back to Login</a>'
    }]
  }]
});
