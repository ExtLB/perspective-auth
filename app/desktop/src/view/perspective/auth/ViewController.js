Ext.define('Client.view.perspective.auth.ViewController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.perspective.auth',

	routes: {
		'auth(/:{page})': {action: 'authRoute'}
	},
	authRoute:function(params) {
    let me = this;
    let view = me.getView();
    let vm = me.getViewModel();
    let pageView = view.down(`perspective\\.auth\\.${params.page}`);
    if (pageView) {
      view.setActiveItem(pageView);
    }
	},

  onAuthViewInitialize: function () {
    let me = this;
    let view = me.getView();
    let vm = view.getViewModel();
    // setTimeout(() => {
    //   vm.getStore('desktopmenu').load();
    // }, 1);
  }

//	onActionsViewLogoutTap: function( ) {
//		var vm = this.getViewModel();
//		vm.set('firstname', '');
//		vm.set('lastname', '');
//
//		Session.logout(this.getView());
//		this.redirectTo(AppCamp.getApplication().getDefaultToken().toString(), true);
//	}

});
