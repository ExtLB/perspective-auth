Ext.define('Client.view.perspective.auth.LockScreen', {
  extend: 'Client.view.perspective.auth.Base',
  xtype: 'perspective.auth.lockscreen',

  requires: [
    'Ext.field.Text',
    'Ext.data.validator.Presence'
  ],

  items: [{
    xtype: 'panel',
    bodyPadding: 20,
    header: {
      userCls: 'perspective-auth-lockscreen-header',
      padding: '10 20',
      title: {
        text: '<b>Goff Smith</b><br>Project Manager',
        icon: 'resources/images/user-profile/2.png'
      }
    },
    defaults: {
      margin: '0 0 10 0'
    },
    items: [{
      xtype: 'component',
      html: 'It\'s been awhile.  Please enter your password to resume',
      margin: '0 0 20 0'
    }, {
      xtype: 'passwordfield',
      reference: 'unlockPassword',
      validators: 'presence',
      required: true,
      placeholder: 'Password'
    }, {
      xtype: 'button',
      text: 'Login',
      iconAlign: 'right',
      iconCls: 'x-fa fa-angle-right',
      width: '100%',
      ui: 'gray-button',
      handler: 'unlockHandler'
    }, {
      xtype: 'component',
      margin: 0,
      html: '<a href="#auth/login" class="perspective-auth-link">Sign in using a different account</a>'
    }]
  }]
});
