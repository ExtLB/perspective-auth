Ext.define('Client.view.perspective.auth.ViewModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.perspective.auth',
  // requires: [
  //   'Client.model.MainNavigationItem'
  // ],
	data: {
		name: 'Client',
    apiUrl: '/api/v1',
    hideRequestId: true,
    requestId: null,
    errorCode: 500,
    errorCodeMessage: '',
    user: {
		  username: 'Username',
    },
	},
  formulas: {
	  hideRequestId: function(get) {
	    return get('requestId') === null ? true : false;
    }
  },
	stores: {
    // notifications: {
		//   autoLoad: true,
     //  model: 'Client.model.MainNotifications',
     //  proxy: {
		// 	  type: 'rest',
     //    url: '{apiUrl}/MainNotifications',
     //    reader: {
		// 	    type: 'json'
     //    }
     //  }
		// }
	}
});
