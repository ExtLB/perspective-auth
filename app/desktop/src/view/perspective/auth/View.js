Ext.define('Client.view.perspective.auth.View', {
	extend: 'Ext.Container',
	xtype: 'perspective.auth',
	controller: {type: 'perspective.auth'},
	viewModel: {type: 'perspective.auth'},
  requires: [
    'Ext.layout.Card',
    'Ext.util.History',
    'Ext.Button',
    'Ext.Label'
  ],
	layout: 'card',
  cls: 'perspective-auth',
  listeners: {
	  initialize: 'onAuthViewInitialize'
  },
  items: [{
	  xtype: 'perspective.auth.login',
    itemId: 'login'
  }, {
	  xtype: 'perspective.auth.lockscreen',
    itemId: 'lockscreen'
  }, {
	  xtype: 'perspective.auth.passwordreset',
    itemId: 'passwordreset'
  }, {
	  xtype: 'perspective.auth.register',
    itemId: 'register'
  }]
});
